# Sven Co-op Models

This repo contains the source files to the models I've made for the Half-Life mod Sven
Co-op.

You can compile them yourself with Crowbar and the Sven SDK, or you can download all of
them pre-built from Releases.

Either way, I hope they're fun ^-^

## Note

You can't compile the models as they are in this repo. After downloading the source, you
should first extract the ZIP file labeled `anims.zip` in each of the models' folders.
Then you should be able to compile them just fine!

The reason for this is that bulk-uploading files to Codeberg (or... gitea in general)
is a pain in the ASS and I don't want to upload all of those animations 5 at a time.
I hope you understand :P